import React, {FunctionComponent} from 'react'
import {Table} from "reactstrap"
import { RouteComponentProps } from 'react-router';
import * as MuscleGroupStore from '../store/MuscleGroup';
import {connect} from 'react-redux';
import { ApplicationState } from '../store';



type MuscleGroupProps = MuscleGroupStore.MuscleGroupState&
    typeof MuscleGroupStore.actionCreators &
    RouteComponentProps<{}>;

export class MuscleGroup extends React.PureComponent<MuscleGroupProps>{

    

        _changeLocation=(musclegroupType: MuscleGroupStore.MuscleGroupType)=>{
            this.props.history.push(`/Exercises/${musclegroupType.id}`); 
            
        };

        public componentDidMount(){
            this.ensureDataFetched();
        }
        private ensureDataFetched(){
            this.props.requestMuscleGroup();
        }

        render(){
            return(
            <>
                <h1>Choose your workout group</h1>
                <Table striped hover size="tabelLabel">
                    <thead>
                        <tr>
                            <th>MuscleGroup</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.muscleGroup.map((muscleGroupType: MuscleGroupStore.MuscleGroupType) =>
                        <MuscleGroupTypeDataRow
                        muscleGroupType ={muscleGroupType} 
                        key ={muscleGroupType.id}
                        onClickHandler={() => this._changeLocation(muscleGroupType)}/>)}
                    </tbody>
                </Table>
            </>          
            );
        }
    }

  export default connect(
      (state: ApplicationState) => state.musclegroup,
      MuscleGroupStore.actionCreators
  )(MuscleGroup as any)


type MuscleGroupTypeDataProps={muscleGroupType: MuscleGroupStore.MuscleGroupType, onClickHandler: () => void}

const MuscleGroupTypeDataRow: FunctionComponent<MuscleGroupTypeDataProps> = (props) =>(
    <tr style= {{cursor: 'pointer'}}  onClick={props.onClickHandler}>
        <td>{props.muscleGroupType.name}</td>
    </tr>
);
