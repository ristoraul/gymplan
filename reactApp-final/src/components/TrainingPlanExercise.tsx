import { FunctionComponent} from 'react';
import * as React from 'react';
import { Table } from "reactstrap";
import { RouteComponentProps } from 'react-router';
import * as TrainingPlanExerciseStore from '../store/TrainingPlanExercise';
import { ApplicationState } from '../store';
import { connect } from 'react-redux';
import Button from 'reactstrap/lib/Button';
import * as ExerciseStore from '../store/Exercises';




type TrainingPlanExerciseProps =
    TrainingPlanExerciseStore.TrainingPlanExerciseState &
    typeof TrainingPlanExerciseStore.actionCreators &    
    RouteComponentProps<{trainingPlanId:string, exerciseId: any}>;

    type ExerciseProps =
    ExerciseStore.ExerciseState &
    typeof ExerciseStore.actionCreators &    
    RouteComponentProps<{muscleGroupId: string, trainingPlanId: string}>;

export class TrainingPlanExercise extends React.PureComponent<TrainingPlanExerciseProps> {

    public componentDidMount(){
        this.ensureDataFetched();
    }

    private ensureDataFetched(){
        this.props.requestTrainingPlanExercises(+this.props.match.params.trainingPlanId)
    }
    _changeLocation = (exercise: ExerciseStore.ExerciseType) =>{
        this.props.history.push(`/exercise/${exercise.id}`);
    }

    public render() {

        return (
          
            <React.Fragment>
                <h1>Training Plan </h1>

                <Table striped hover aria-labelledby="tabelLabel">
                    <thead>
                        <tr>
                            <th>Exercise Id</th>
                            <th>Name</th>
                            <th>Show more details</th>
                            <th>Remove exercise</th>                
                        </tr>
                    </thead>              
                    <tbody> 
                        
                        {this.props.trainingPlanExercise.map((trainingPlanExerciseType: TrainingPlanExerciseStore.TrainingPlanExerciseType) =>
                    <TrainingPlanExerciseDataRow
                    trainingPlanExerciseType ={trainingPlanExerciseType}
                    key ={trainingPlanExerciseType.id}
                    onDeleteHandler={()=> this.props.deleteExerciseFromTrainingPlan(+this.props.match.params.trainingPlanId, trainingPlanExerciseType.id)}
                    onClickHandlerDetails ={() => this._changeLocation(trainingPlanExerciseType.exercise)}
                    
                    />
                        )}                                   
                    </tbody>
                </Table>
                
            </React.Fragment>
        );
    }
}
export default connect(
    (state: ApplicationState) => state.trainingPlanExercise,
    TrainingPlanExerciseStore.actionCreators
)(TrainingPlanExercise as any)

type TrainingPlanExerciseDataProps = { trainingPlanExerciseType: TrainingPlanExerciseStore.TrainingPlanExerciseType, onDeleteHandler: () => void,
    onClickHandlerDetails: ()=> void  }

const TrainingPlanExerciseDataRow: FunctionComponent<TrainingPlanExerciseDataProps> = (props) => (
  <tr>
        <td>{props.trainingPlanExerciseType.id}</td>
        <td>{props.trainingPlanExerciseType.name}</td>
        <td><Button color="info" onClick={props.onClickHandlerDetails}>Details</Button></td>     
        <td><Button color="danger" onClick ={props.onDeleteHandler} >X</Button></td>  
    </tr>
);



