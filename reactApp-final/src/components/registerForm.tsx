import React, {useState, useEffect} from 'react'
import Form from 'reactstrap/lib/Form'
import FormGroup from 'reactstrap/lib/FormGroup'
import Label from 'reactstrap/lib/Label'
import Input from 'reactstrap/lib/Input'
import { Button } from 'reactstrap'
import {actionCreators} from '../store/Participant'



const RegisterForm= () =>{
  const [firstNameValue, setFirstName] = useState("")
  const [lastNameValue, setLastName] = useState("")
  const [languageValue, setLanguage] = useState("")
  const [formError, setFormError] = useState("");


  const handleFirstNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFirstName(e.target.value);
  };
  const handleLastNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLastName(e.target.value);
  };
  const handleLanguageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLanguage(e.target.value);
  };
  
  const handleAddClick = () => {
    setFormError("")
    if (firstNameValue !== "" && lastNameValue !== "" && languageValue !== ""){
      actionCreators.addParticipant(firstNameValue, lastNameValue, languageValue)
      setFirstName("");
      setLastName("");
      setLanguage("");
    }
    else{
      setFormError("Insufficient data");
    }

  }
    return (
        <Form
          style={{
            margin: "10px 0px"
          }}
          onSubmit={e => e.preventDefault()}
        >
          <div>
            <FormGroup>
              <Label for="participant_firstName">FirstName</Label>
              <Input
                type="text"
                name="name"
                value={firstNameValue}
                onChange={handleFirstNameChange}
                id="participant_firstName"
                placeholder="Mari"
            />
            </FormGroup>
            <FormGroup>
            <Label for="participant_LastName">LastName</Label>
              <Input
                  type="text"
                  name="name"
                  value={lastNameValue}
                  onChange={handleLastNameChange}
                  id="participant_lastName"
                  placeholder="Mets"
              />
            </FormGroup>
            <FormGroup>
              <Label for="participant_reps">Language</Label>
              <Input
                type="select"
                name="name"
                value={languageValue}
                onChange={handleLanguageChange}
                id="exerciseReps"
                placeholder="10"
              />
              <option></option>
            </FormGroup>
          
          </div>
          <Button color="primary" onClick = {handleAddClick}>
            ADD
          </Button>
        </Form>
    );};
        
    export default RegisterForm;
