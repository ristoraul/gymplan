import { FunctionComponent} from 'react';
import * as React from 'react';
import { Table } from "reactstrap";
import { RouteComponentProps } from 'react-router';
import * as ExerciseStore from '../store/Exercises';
import { ApplicationState } from '../store';
import { connect } from 'react-redux';
import Button from 'reactstrap/lib/Button';



type ExerciseProps =
    ExerciseStore.ExerciseState &
    typeof ExerciseStore.actionCreators &    
    RouteComponentProps<{}>;

export class Exercise extends React.PureComponent<ExerciseProps> {

    public componentDidMount(){
        this.ensureDataFetched();
    }

    private ensureDataFetched(){
       this.props.requestExercisesList();
       
    } 
    _changeLocation = (exercise: ExerciseStore.ExerciseType) =>{
        this.props.history.push(`/exercise/${exercise.id}`);
    }

    public render() {
        /*let muscleGroupId = + this.props.location.pathname.charAt(10)+ this.props.location.pathname.charAt(11);    
        let exercises = this.props.exercise.filter(x=>x.muscleGroupId === + muscleGroupId);*/
        return (
            <React.Fragment>
                <h1>Exercises</h1>

                <Table striped hover aria-labelledby="tabelLabel">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Add to your training plan</th> 
                            <th>Show more details</th>
                            <th>Delete</th>   
                            <th></th>                      
                        </tr>
                    </thead>              
                    <tbody> 
                        
                        {this.props.exercise.map((exerciseType: ExerciseStore.ExerciseType) =>
                    <ExerciseDataRow
                    exerciseType ={exerciseType}
                    key ={exerciseType.id}
                    onDeleteHandler={()=> this.props.deleteExercise(exerciseType.muscleGroupId,exerciseType.id)}
                    onClickHandler={() => this.props.addExerciseToTrainingPlan(exerciseType.id, 1)}
                    onClickHandlerDetails ={() => this._changeLocation(exerciseType)}
                    />
                        )}                                   
                    </tbody>
                </Table>
                
            </React.Fragment>
        );
    }
}
export default connect(
    (state: ApplicationState) => state.exercises,
    ExerciseStore.actionCreators
)(Exercise as any)

function addingAlertFunction(){
    alert('Exercise has been added to your training plan')
}

type ExerciseDataProps = { exerciseType: ExerciseStore.ExerciseType, onDeleteHandler: () => void, onClickHandler: () => void,
    onClickHandlerDetails: ()=> void }

const ExerciseDataRow: FunctionComponent<ExerciseDataProps> = (props) => (
  <tr>
        <td>{props.exerciseType.id}</td>
        <td>{props.exerciseType.name}</td>
        <td><Button color="success" onClick={props.onClickHandler} onClickCapture={addingAlertFunction}>Add</Button></td>
        <td><Button color="info" onClick={props.onClickHandlerDetails}>Details</Button></td>    
        <td><Button color="danger" onClick={props.onDeleteHandler}>X</Button></td>
        
    </tr>
);


