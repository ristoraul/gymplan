import * as ExerciseStore from '../store/Exercises'
import { Table,Button } from "reactstrap";
import { RouteComponentProps } from 'react-router'
import * as React from 'react';
import { ApplicationState }from'../store';
import { connect } from 'react-redux';
import { FunctionComponent} from 'react';


type ExerciseProps =
    ExerciseStore.ExerciseState &
    typeof ExerciseStore.actionCreators &    
    RouteComponentProps<{exerciseId: any}>;

class ExerciseDetails extends React.PureComponent<ExerciseProps>{
    
    

public render(){
    let exerciseId = this.props.match.params.exerciseId;
    let exercises = this.props.exercise.filter(x=>x.id == +exerciseId);
    let exercise = exercises[0];
    if (exercise === undefined){
        return <h1>Exercise not found</h1>; 
    }
    return(
        <React.Fragment>
        <h1> {exercise.name}</h1>
        <Table striped hover aria-labelledby="tabelLabel">
        <thead>
                        <tr>
                            <th>Exercise name</th>
                            <th>Sets</th>
                            <th>Reps</th>  
                            <th>Description</th>                                        
                        </tr>
                    </thead>              
                    <tbody> 
                        
                        {this.props.exercise.filter(x=>x.id == +exerciseId).map((exerciseType: ExerciseStore.ExerciseType) =>
                    <ExerciseDetailsDataRow
                    exerciseType ={exerciseType}
                    key ={exerciseType.id}
                    
                  
                    />
                        )}                                   
                    </tbody>
        </Table>
</React.Fragment>
    )
}
}
export default connect(
    (state: ApplicationState) => state.exercises,
    ExerciseStore.actionCreators
)(ExerciseDetails as any)

type ExerciseDetailsDataProps = { exerciseType: ExerciseStore.ExerciseType}
    
    const ExerciseDetailsDataRow: FunctionComponent<ExerciseDetailsDataProps> = (props) => (
      <tr>
            
            <td>{props.exerciseType.name}</td>
            <td>{props.exerciseType.exerciseSets}</td>
            <td>{props.exerciseType.exerciseReps}</td>
            <td>{props.exerciseType.description}</td>
        </tr>
    );
    