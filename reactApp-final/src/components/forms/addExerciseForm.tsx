import React from 'react'
import Form from 'reactstrap/lib/Form'
import FormGroup from 'reactstrap/lib/FormGroup'
import Label from 'reactstrap/lib/Label'
import Input from 'reactstrap/lib/Input'
import { Button } from 'reactstrap'

const addExerciseForm = () =>{
    return (
        <Form
          style={{
            margin: "10px 0px"
          }}
          //onSubmit={e => e.preventDefault()}
        >
          <div>
            <FormGroup>
              <Label for="exercise_name">Name</Label>
              <Input
                type="text"
                name="name"
                id="exercise_name"
                placeholder="Exercise name"
            />
            </FormGroup>
            <FormGroup>
            <Label for="exercise_sets">Sets</Label>
              <Input
                  type="text"
                  name="name"
                  id="exerciseSets"
                  placeholder="3"
              />
            </FormGroup>
            <FormGroup>
              <Label for="exercise_reps">Reps</Label>
              <Input
                type="text"
                name="name"
                id="exerciseReps"
                placeholder="10"
              />
            </FormGroup>
          
          </div>
          <Button color="primary">
            ADD
          </Button>
        </Form>
    );};
    export default addExerciseForm;
