import React, {FunctionComponent} from 'react'
import {Table} from "reactstrap"
import { RouteComponentProps } from 'react-router'




type ProfileType = {
    id: number;
    name: string;
    email: string;
    gender: string;
    age: number;
    weight: number
    ;
}
 
type ProfileState = {
    profile: ProfileType[]
}

export class Profile extends React.PureComponent<{}, ProfileState>{
    state = {
        profile: [
            {id: 1, name: "Toni Kutsar", email: "tokuts@ttu.ee",gender: "male",age: 22, weight: 80.2}
        ]
    }

    render(){
        return (
            <>
            <h1>Profile</h1>
            <Table striped hover size="sn">
                <thead>
                    <tr>      

                        <th>Name</th>
                
                        <th>Email</th>
            
                        <th>Gender</th>
                    
                        <th>Age</th>
                   
                        <th>Weight</th>
             
                    </tr>

                    
                </thead>
                <tbody>
                    {this.state.profile.map((profile: ProfileType) => <ProfileDataRow profile={profile} key={profile.id}/>)}
                </tbody>
            </Table>
            </>
        );
    }
}

type ProfileDataProps = { profile: ProfileType}
const ProfileDataRow: FunctionComponent<ProfileDataProps> = (props) => (
    <tr>
   
    <td>{props.profile.name}</td>
    <td>{props.profile.email}</td>
    <td>{props.profile.gender}</td>
    <td>{props.profile.age}</td>
    <td>{props.profile.weight}</td>

    </tr>
);
export default Profile;