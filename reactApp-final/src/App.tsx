import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import './custom.css'

import MuscleGroup from './components/MuscleGroup';
import Exercise from './components/Exercise';
import Profile from './components/Profile'
import TrainingPlanExercise from './components/TrainingPlanExercise';
import ExerciseList from './components/ExerciseList';
import ExerciseDetails from './components/ExerciseDetails';
import addExerciseForm from './components/forms/addExerciseForm';
import Participant from './components/Participant';
import RegisterForm from './components/registerForm';

export default () => (
    <Layout>
        <Route exact path='/' component={Home} />
        <Route exact path = '/MuscleGroup' component={MuscleGroup}/>
        <Route exact path = '/ExerciseList' component={ExerciseList}/>
        <Route exact path='/Exercises/:muscleGroupId' component={Exercise}/> 
        <Route exact path='/Profile' component={Profile}/> 
        <Route exact path ='/TrainingPlan/:trainingPlanId' component={TrainingPlanExercise}/>
        <Route path= '/Exercise/:exerciseId' component={ExerciseDetails}/>
        <Route path= '/addExerciseForm' component={addExerciseForm}/>
        <Route path= '/registerForm' component={Participant}/>

    </Layout>
);
