import * as Exercise from './Exercises'
import * as MuscleGroup from './MuscleGroup';
import * as TrainingPlanExercise from './TrainingPlanExercise'
import * as Participant from './Participant'


// The top-level state object
export interface ApplicationState { 
    exercises: Exercise.ExerciseState | undefined;
    musclegroup: MuscleGroup.MuscleGroupState | undefined;
    trainingPlanExercise: TrainingPlanExercise.TrainingPlanExerciseState | undefined;
    participant: Participant.ParticipantState | undefined

}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {

    exercises: Exercise.reducer,
    musclegroup: MuscleGroup.reducer,
    trainingPlanExercise: TrainingPlanExercise.reducer,
    participant: Participant.reducer

};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
