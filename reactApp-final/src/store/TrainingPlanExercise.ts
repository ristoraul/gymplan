import { Reducer, Action } from 'redux';
import { AppThunkAction } from '.';
import { Exercise } from '../components/Exercise';
import { ExerciseType } from './Exercises';

export type TrainingPlanExerciseType = {
    
    id: number; //exerciseId
    trainingPlanId: number;
    name: string;
    muscleGroupId: number;
    exercise: ExerciseType
    
}
export type TrainingPlanExerciseState = {
    trainingPlanExercise: TrainingPlanExerciseType[]

}

export const unloadedState: TrainingPlanExerciseState = {
    trainingPlanExercise: [],   
};

export interface RequestTrainingExerciseAction {
    type : "REQUEST_TRAININGPLAN_EXERCISE";
}

export interface ReceiveTrainingExerciseAction {
    type : "RECEIVE_TRAININGPLAN_EXERCISE";
    trainingPlanExercise : TrainingPlanExerciseType[];
}
interface DeleteExerciseFromTrainingPlanAction{
    type: "DELETE_EXERCISE_FROM_TRAININGPLAN";
    exerciseId: number;
}

type KnownAction = ReceiveTrainingExerciseAction | RequestTrainingExerciseAction | DeleteExerciseFromTrainingPlanAction;


export const actionCreators = {
    deleteExerciseFromTrainingPlan:(trainingPlanId :number, exerciseId: number): AppThunkAction<KnownAction> =>(dispatch, getState) =>{
        const appState = getState();
        if(appState && appState.exercises){
            fetch(`api/trainingplan/${trainingPlanId}/exercises/${exerciseId}`,{
                method: "DELETE"
            })
            .then(response => response.json())
            .then(data => {
                dispatch({type: "DELETE_EXERCISE_FROM_TRAININGPLAN", exerciseId: exerciseId});
            });
        }
    },
        
    requestTrainingPlanExercises: (trainingPlanId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.trainingPlanExercise){
           dispatch({type:"REQUEST_TRAININGPLAN_EXERCISE"})
            fetch(`api/trainingplan/${trainingPlanId}/exercises`)
            .then(response => response.json() as Promise<TrainingPlanExerciseType[]>)
            .then(data => {
                dispatch({type: "RECEIVE_TRAININGPLAN_EXERCISE", trainingPlanExercise:data});
            });
        }
    }
};
export const reducer: Reducer<TrainingPlanExerciseState> = (
    state: TrainingPlanExerciseState | undefined,
    incomingAction: Action
    ): TrainingPlanExerciseState => {
    if ( state === undefined){
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type){
        case "DELETE_EXERCISE_FROM_TRAININGPLAN":
            return{
                trainingPlanExercise: state.trainingPlanExercise.filter(x=>x.id !== action.exerciseId)
            };
        case "REQUEST_TRAININGPLAN_EXERCISE":
            return {
                trainingPlanExercise:state.trainingPlanExercise,
              
            };
        case "RECEIVE_TRAININGPLAN_EXERCISE":
            return {
                trainingPlanExercise: action.trainingPlanExercise,
               
            };          
            default:
                return state;
    }



    
};