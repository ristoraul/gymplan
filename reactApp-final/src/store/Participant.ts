import {AppThunkAction} from '.';
import {Reducer, Action} from 'redux';
import axios from 'axios'

export type ParticipantType = {
    id: number;
    firstName: string;
    lastName: number;
    language: string;   
}
export type ParticipantState = {
    participant: ParticipantType[]
}
export const unloadedState: ParticipantState = {
   participant:[]

};

interface AddParticipant {
    type: "ADD_PARTICIPANT";
    participant:ParticipantType[];
} 
interface RequestParticipants {
    type : "REQUEST_PARTICIPANTS";
}

interface ReceiveParticipants {
    type : "RECEIVE_PARTICIPANTS";
    participant : ParticipantType[];
}

type KnownAction = AddParticipant | RequestParticipants | ReceiveParticipants;
export const FETCH_STUDENTS_START = () => ({
    type: "FETCH_STUDENTS_START" //START action ei saada andmeid, seega pole payload vajalik
  });
  
  /*export const FETCH_STUDENTS_SUCCESS = (payload: any) => ({
    type: "FETCH_STUDENTS_SUCCESS",
    payload: payload
  });
  
  export const FETCH_STUDENTS_ERROR = (payload: any) => ({
    type: "FETCH_STUDENTS_ERROR",
    payload: payload
  });
  
  export const SELECT_STUDENT = (payload: any) => ({
    type: "SELECT_STUDENT",
    payload: payload
  });

export const addNewParticipant = async (firstName: string, lastName: string, language: string) => {
    const participantToAdd = {"Firstname": firstName, "Lastname": lastName, "Language": language};
    console.log(participantToAdd);
    return await axios.post(`/api/participants`, participantToAdd);}

    export const getAllParticipants: (dispatch: any) => void = (dispatch) => {
        dispatch(FETCH_STUDENTS_START());
        fetchAllParticipants().then(data => dispatch(FETCH_STUDENTS_SUCCESS(data.data))).catch(err => dispatch(FETCH_STUDENTS_ERROR(err.message)))
    };
    
    
    export const fetchAllParticipants = () => {
        return axios.get(`/api/participants`);
    };*/

export const actionCreators = {
    

addParticipant: (firstName: any, lastName: any, language: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
    const appState =getState();
    if (appState && appState.participant){
        fetch(`api/participants/`,{
            method: 'POST',
            headers: {'Content-Type': 'application/json; charset=utf-8'},

            body: JSON.stringify({})

        })
        .then(response => response.json())
         .then(data => {
             dispatch({type: "ADD_PARTICIPANT", participant:data});
         });
    }
    
    },
    requestParticipants: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if ( appState && appState.participant){
            dispatch ({type:"REQUEST_PARTICIPANTS"})
            fetch(`api/participants`)
            .then(response => response.json() as Promise<ParticipantType[]>)
            .then(data => {
                dispatch({type: "RECEIVE_PARTICIPANTS", participant:data});
            });
        }
    }}
    export const reducer: Reducer<ParticipantState> = (
        state: ParticipantState| undefined,
        incomingAction: Action
        ):
        ParticipantState => {
            if (state === undefined){
                return unloadedState;
            }
            const action = incomingAction as KnownAction;
    switch (action.type) {
        case "REQUEST_PARTICIPANTS":
            return {
                participant:state.participant,
               // isLoading: true
            };
        case "RECEIVE_PARTICIPANTS":
            return {
                participant: action.participant,
               // isLoading:false
            };          
            default:
                return state;
        }
    }
