import { Reducer, Action } from 'redux';
import { AppThunkAction } from '.';

export type ExerciseType = {
    id: number;
    name: string;
    muscleGroupId: number;
    exerciseSets: string;
    exerciseReps: string;
    description: string;    
}
export type ExerciseState = {
    exercise: ExerciseType[]

}

export const unloadedState: ExerciseState = {
    exercise: []   
};
interface DeleteExerciseAction{
    type: "DELETE_EXERCISE";
    exerciseId: number;
}
interface RequestExerciseAction {
    type : "REQUEST_EXERCISE";
}

interface ReceiveExerciseAction {
    type : "RECEIVE_EXERCISE";
    exercise : ExerciseType[];
}
interface RequestExerciseListAction {
    type : "REQUEST_EXERCISE_LIST";
}

interface ReceiveExerciseListAction {
    type : "RECEIVE_EXERCISE_LIST";
    exercise : ExerciseType[];
}
interface PostExerciseToTrainingPlanAction {
    type: "POST_EXERCISE_TO_TRAININGPLAN";
    exercise:ExerciseType[];   
}
interface ReceiveExerciseByIdAction{
    type: "RECEIVE_EXERCISE_BY_ID";
    exercise:ExerciseType[]
}
interface PostExerciseAction {
    type: "POST_EXERCISE";
    exercise:ExerciseType[];   
}


type KnownAction = DeleteExerciseAction | ReceiveExerciseAction | RequestExerciseAction
|ReceiveExerciseListAction | RequestExerciseListAction | PostExerciseToTrainingPlanAction | ReceiveExerciseByIdAction | PostExerciseAction;


export const actionCreators = {
     deleteExercise:(muscleGroupId:number, exerciseId: number): AppThunkAction<KnownAction> =>(dispatch, getState) =>{
         const appState = getState();
         if(appState && appState.exercises){
             fetch(`api/musclegroup/${muscleGroupId}/exercise/${exerciseId}`,{
                 method: "DELETE"
             })
             .then(response => response.json())
             .then(data => {
                 dispatch({type: "DELETE_EXERCISE", exerciseId: exerciseId});
             });
         }
     },
    requestExercises: (muscleGroupId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.exercises){
            dispatch({ type: "REQUEST_EXERCISE" })
            fetch(`api/musclegroup/${muscleGroupId}/exercise`)
            .then(response => response.json() as Promise<ExerciseType[]>)
            .then(data => {
                dispatch({type: "RECEIVE_EXERCISE", exercise:data});
            });
        }
    },
    requestExercisesList: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.exercises){
            dispatch({ type: "REQUEST_EXERCISE" })
            fetch(`api/musclegroupexercise/getexercises`)
            .then(response => response.json() as Promise<ExerciseType[]>)
            .then(data => {
                dispatch({type: "RECEIVE_EXERCISE", exercise:data});
            });
        }
    },
    requestExercise: (exerciseId: number):AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.exercises){
            dispatch({ type: "REQUEST_EXERCISE" })
            fetch(`api/musclegroupexercise/${exerciseId}`)
            .then(response => response.json() as Promise<ExerciseType[]>)
            .then(data => {
                dispatch({type: "RECEIVE_EXERCISE_BY_ID", exercise:data});
            });
        }
    },
    addExerciseToTrainingPlan: (exerciseId: any, trainingPlanId: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState =getState();
        if (appState && appState.exercises){
            fetch(`api/musclegroupexercise/${exerciseId}/trainingplans/${trainingPlanId}`,{
                method: 'POST',
                headers: {'Content-Type': 'application/json; charset=utf-8'},

                body: JSON.stringify({})

            })
            .then(response => response.json())
             .then(data => {
                 dispatch({type: "POST_EXERCISE_TO_TRAININGPLAN", exercise:data});
             });
        }
        },
        addExercise: (muscleGroupId: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
            const appState =getState();
            if (appState && appState.exercises){
                fetch(`api/musclegroup/${muscleGroupId}/exercise`,{
                    method: 'POST',
                    headers: {'Content-Type': 'application/json; charset=utf-8'},
    
                    body: JSON.stringify({})
    
                })
                .then(response => response.json())
                 .then(data => {
                     dispatch({type: "POST_EXERCISE", exercise:data});
                 });
            }
        }
    }


export const reducer: Reducer<ExerciseState> = (
    state: ExerciseState | undefined,
    incomingAction: Action
    ): ExerciseState => {
    if ( state === undefined){
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type){
        case "RECEIVE_EXERCISE_BY_ID":
            return{
                exercise:action.exercise,
            };
        case "DELETE_EXERCISE":
            return{
                exercise: state.exercise.filter(x=>x.id !== action.exerciseId)
            }
        case "REQUEST_EXERCISE":
            return {
                exercise:state.exercise,
            };
        case "RECEIVE_EXERCISE":
            return {
                exercise: action.exercise,
            };
        case "RECEIVE_EXERCISE_LIST":
            return {
                exercise: action.exercise
            }
        case "REQUEST_EXERCISE_LIST":
            return {
                exercise: state.exercise
            }        
            default:
                return state;
    }
    
};