import { Reducer, Action } from 'redux';
import MuscleGroup from '../components/MuscleGroup';
import { AppThunkAction } from '.';


export type MuscleGroupType = {
    id: number;
    name: string;
   
}
export type MuscleGroupState = {
    muscleGroup: MuscleGroupType[]
}

interface RequestMuscleGroupAction {
    type : "REQUEST_MUSCLEGROUP";
}

interface ReceiveMuscleGroupAction {
    type : "RECEIVE_MUSCLEGROUP";
    muscleGroup : MuscleGroupType[];
}

type KnownAction = ReceiveMuscleGroupAction | RequestMuscleGroupAction;

export const unloadedState: MuscleGroupState = {
   muscleGroup:[],
  // isLoading: false

};

export const actionCreators = {
    requestMuscleGroup: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if ( appState && appState.musclegroup){
            dispatch ({type:"REQUEST_MUSCLEGROUP"})
            fetch(`api/musclegroup`)
            .then(response => response.json() as Promise<MuscleGroupType[]>)
            .then(data => {
                dispatch({type: "RECEIVE_MUSCLEGROUP", muscleGroup:data});
            });
        }
    }
};

export const reducer: Reducer<MuscleGroupState> = (
    state: MuscleGroupState| undefined,
    incomingAction:Action
    ): MuscleGroupState => {
    if (state === undefined){
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case "REQUEST_MUSCLEGROUP":
            return {
                muscleGroup:state.muscleGroup,
               // isLoading: true
            };
        case "RECEIVE_MUSCLEGROUP":
            return {
                muscleGroup: action.muscleGroup,
               // isLoading:false
            };          
            default:
                return state;

        }
};
