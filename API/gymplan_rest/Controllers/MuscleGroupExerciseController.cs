﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using gymplan_rest.Model;
using Newtonsoft.Json.Linq;

namespace gymplan_rest.Controllers
{
    [Route("api/[controller]")]
    [Route("api/musclegroup/{musclegroupId:int}/exercise")]
    
    [ApiController]
    public class MuscleGroupExerciseController : ControllerBase
    {
        private readonly DataContext _context;

        public MuscleGroupExerciseController(DataContext context)
        {
            _context = context;
        }

        // GET: api/MuscleGroupExercise
       /* [HttpGet]
        public async Task<ActionResult<IEnumerable<Exercise>>> GetExerciseList()
        {
            return await _context.ExerciseList.ToListAsync();
        }*/

        // GET: api/MuscleGroupExercise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Exercise>>> GetExerciseList(int musclegroupId)
        {
            var musclegroup = await _context.MuscleGroupList.FindAsync(musclegroupId);
            if (musclegroup == null)
            {
                return NotFound();
            }
            return await _context.ExerciseList.Where(x => x.MuscleGroupId == musclegroupId).ToListAsync();
        }


        [HttpGet("GetExercises")]
        public async Task<ActionResult<IEnumerable<Exercise>>> GetExercises()
        {
            return await _context.ExerciseList.ToListAsync();
        }

        // GET: api/MuscleGroupExercise/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Exercise>> GetExercise(int id)
        {
            var exercise = await _context.ExerciseList.FindAsync(id);

            if (exercise == null)
            {
                return NotFound();
            }

            return exercise;
        }

        // PUT: api/MuscleGroupExercise/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExercise(int id, Exercise exercise)
        {
            if (id != exercise.Id)
            {
                return BadRequest();
            }

            _context.Entry(exercise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExerciseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MuscleGroupExercise
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Exercise>> PostExercise(int musclegroupId, Exercise exercise)
        {
            var musclegroup = await _context.MuscleGroupList.FindAsync(musclegroupId);
            if (musclegroup == null)
            {
                return NotFound();
            }

            exercise.MuscleGroupId = musclegroupId;
            _context.ExerciseList.Add(exercise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetExercise", new { musclegroupId, id = exercise.Id }, exercise);
        }

       
        [HttpPost("{exerciseId}/trainingplans/{trainingPlanId}")]
        public async Task<ActionResult<TrainingPlanExercise>> AddExerciseToTrainingPlan(int exerciseId,int trainingPlanId, [FromBody]  TrainingPlanExercise value)
        {
            var exercise = _context.ExerciseList.Include(x => x.TrainingPlanExercises).FirstOrDefault(x => x.Id == exerciseId);
            value.ExerciseId = exerciseId;
            value.TrainingPlanId = trainingPlanId;

            if (exercise.TrainingPlanExercises == null)
                exercise.TrainingPlanExercises = new List<TrainingPlanExercise>();

            //taaskasutab teist requesti et näha kas on lubatud lisada
            var availableCourses = _context.TrainingPlanList;

            if (availableCourses.Select(x => x.Id).Contains(value.TrainingPlanId))
            {
                exercise.TrainingPlanExercises.Add(value);
                await _context.SaveChangesAsync();
            }
            else
            {
                return Ok("Training plan not available");
            }
            return value;
        }
        // DELETE: api/MuscleGroupExercise/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Exercise>> DeleteExercise(int id)
        {
            var exercise = await _context.ExerciseList.FindAsync(id);
            if (exercise == null)
            {
                return NotFound();
            }

            _context.ExerciseList.Remove(exercise);
            await _context.SaveChangesAsync();

            return exercise;
        }

        private bool ExerciseExists(int id)
        {
            return _context.ExerciseList.Any(e => e.Id == id);
        }
    }
}
