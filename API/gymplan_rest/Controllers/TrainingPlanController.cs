﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using gymplan_rest.Model;

namespace gymplan_rest.Controllers
{
    [Route("api/trainingplan/")]
    [ApiController]
    public class TrainingPlanController : ControllerBase
    {
        private readonly DataContext _context;

        public TrainingPlanController(DataContext context)
        {
            _context = context;
        }

        // GET: api/TrainingPlan
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TrainingPlan>>> GetTrainingPlanList()
        {
            return await _context.TrainingPlanList.ToListAsync();
        }

        // GET: api/TrainingPlan/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TrainingPlan>> GetTrainingPlan(int id)
        {
            var trainingPlan = await _context.TrainingPlanList.FindAsync(id);

            if (trainingPlan == null)
            {
                return NotFound();
            }

            return trainingPlan;
        }

        // GET: api/TrainingPlan/1/exercises
        [HttpGet("{id}/exercises")]
        public async Task<ActionResult<IEnumerable<Exercise>>> GetTrainingPlanExercises(int id)
        {
            var trainingplan = await _context.TrainingPlanList
                .Include(x => x.TrainingPlanExercises)
                .ThenInclude(x => x.Exercise)
                .FirstOrDefaultAsync(x => x.Id == id);

            var exercises = trainingplan.TrainingPlanExercises.Select(x => x.Exercise);

            return exercises.ToList();
        }

        // PUT: api/TrainingPlan/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrainingPlan(int id, TrainingPlan trainingPlan)
        {
            if (id != trainingPlan.Id)
            {
                return BadRequest();
            }

            _context.Entry(trainingPlan).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainingPlanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        // POST: api/TrainingPlan
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TrainingPlan>> PostTrainingPlan(TrainingPlan trainingPlan)
        {
            _context.TrainingPlanList.Add(trainingPlan);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTrainingPlan", new {id = trainingPlan.Id}, trainingPlan);
        }

        // DELETE: api/TrainingPlan/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TrainingPlan>> DeleteTrainingPlan(int id)
        {
            var trainingPlan = await _context.TrainingPlanList.FindAsync(id);
            if (trainingPlan == null)
            {
                return NotFound();
            }

            _context.TrainingPlanList.Remove(trainingPlan);
            await _context.SaveChangesAsync();

            return trainingPlan;
        }

        // DELETE: api/TrainingPlan/1/exercises/1
        [HttpDelete("{trainingPlanId}/exercises/{exerciseId}")]
        public async Task<ActionResult<TrainingPlan>> DeleteExerciseFromTrainingPlan(int trainingPlanId, int exerciseId)
        {
            var trainingPlan = await _context.TrainingPlanList.Include(x => x.TrainingPlanExercises)
                .ThenInclude(x => x.Exercise)
                .FirstOrDefaultAsync(x => x.Id == trainingPlanId);

            if (trainingPlan == null)
            {
                return NotFound();
            }
            var trainingPlanExercises = trainingPlan.TrainingPlanExercises;
            if (exerciseId != null)
            {
                var exercise = trainingPlanExercises.FirstOrDefault(x => x.ExerciseId == exerciseId);
                trainingPlan.TrainingPlanExercises.Remove(exercise);
            }

            await _context.SaveChangesAsync();
            return  trainingPlan;

        }

        private bool TrainingPlanExists(int id)
        {
            return _context.TrainingPlanList.Any(e => e.Id == id);
        }


    }
}
