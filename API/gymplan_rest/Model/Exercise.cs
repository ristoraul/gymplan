﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace gymplan_rest.Model
{
    public class Exercise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MuscleGroupId { get; set; }
        public string? ExerciseSets{ get; set; }
        public string? ExerciseReps { get; set; }
        public string? Description { get; set; }

        [JsonIgnore]
        public virtual ICollection<TrainingPlanExercise> TrainingPlanExercises { get; set; }
    }
}
