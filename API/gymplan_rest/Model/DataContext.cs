﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace gymplan_rest.Model
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        public DbSet<MuscleGroup> MuscleGroupList { get; set; }
        public DbSet<Exercise> ExerciseList { get; set; }
        public DbSet<TrainingPlan> TrainingPlanList { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TrainingPlanExercise>().ToTable("TrainingPlanExercise")
                .HasKey(key => new { key.ExerciseId, key.TrainingPlanId });

            modelBuilder.Entity<Exercise>().ToTable("Exercise").HasKey(x => x.Id);
            modelBuilder.Entity<TrainingPlan>().ToTable("TrainingPlan").HasKey(x => x.Id);
            modelBuilder.Entity<MuscleGroup>().ToTable("MuscleGroup").HasKey(x => x.Id);

            modelBuilder.Entity<TrainingPlan>().HasData(
                new TrainingPlan
                {
                    Id = 1,
                    TrainingPlanCode = "701701998",
                    Name = "My training plan"
                });

            modelBuilder.Entity<MuscleGroup>().HasData(
                new MuscleGroup
                {
                    Id = 1,
                    Name = "Chest",
                },
                new MuscleGroup
                {
                    Id = 2,
                    Name = "Deltoid",
                },
                new MuscleGroup
                {
                    Id = 3,
                    Name = "Abdominal",
                },
                new MuscleGroup
                {
                    Id = 4,
                    Name = "Upper Back",
                },
                new MuscleGroup
                {
                    Id = 5,
                    Name = "Biceps",
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 1,
                    Name = "Barbell bench press",
                    MuscleGroupId = 1,
                    ExerciseSets = "5",
                    ExerciseReps = "5",
                    Description =  
                        "1. Lie on the bench with your eyes under the bar 2. Grab the bar with a medium grip - width(thumbs around the bar!) 3. Unrack the bar by straightening your arms 4. Lower the bar to your mid - chest 5. Press the bar back up until your arms are straight"

                },
                new Exercise
                {
                    Id = 2,
                    Name = "Dumbbell bench press",
                    MuscleGroupId = 1,
                    ExerciseSets = "4",
                    ExerciseReps = "12",
                    Description = "Sit on 70-90 degree bench, elbows bent, press dumbbells up, moving path should be natural"
                },
                new Exercise
                {
                    Id = 3,
                    Name = "Barbell shoulder press",
                    MuscleGroupId = 2,
                    ExerciseSets = "5",
                    ExerciseReps = "5",
                    Description = "Stand up, feet shoulder width apart, hands should grip the bar by shoulders length, glutes activated, press the bar up"
                },
                new Exercise
                {
                    Id = 4,
                    Name = "Seated dumbbell shoulder press",
                    MuscleGroupId = 2
                },
                new Exercise
                {
                    Id = 5,
                    Name = "Leg raises",
                    MuscleGroupId = 3
                },
                new Exercise
                {
                    Id = 6,
                    Name = "Crunches",
                    MuscleGroupId = 3
                },
                new Exercise
                {
                    Id = 7,
                    Name = "Pull ups",
                    MuscleGroupId = 4
                },
                new Exercise
                {
                    Id = 8,
                    Name = "Barbell row",
                    MuscleGroupId = 4
                },
                new Exercise
                {
                    Id = 9,
                    Name = "Barbell shrug",
                    MuscleGroupId = 4
                },
                new Exercise
                {
                    Id = 10,
                    Name = "Standing dumbbell curl",
                    MuscleGroupId = 5,
                    ExerciseSets = "4",
                    ExerciseReps = "12-15",
                    Description = "Focus on the negative movement of the exercise"
                },
                new Exercise
                {
                    Id = 11,
                    Name = "Hammer curl",
                    MuscleGroupId = 5,
                    ExerciseReps = "15-20",
                    ExerciseSets = "3",
                    Description = "Hold dumbbells upright in hand"
                },
                new Exercise
                {
                    Id = 12,
                    Name = "Ez-bar biceps curl",
                    MuscleGroupId = 5,
                    ExerciseSets = "5",
                    ExerciseReps = "10-12",
                    Description = "Focus on biceps contraction"
                }
            );
        }
    }
}
