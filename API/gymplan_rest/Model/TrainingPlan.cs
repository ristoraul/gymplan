﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace gymplan_rest.Model
{
    public class TrainingPlan
    {
        public int Id { get; set; }
        public string TrainingPlanCode { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<TrainingPlanExercise> TrainingPlanExercises { get; set; }

    }
}
