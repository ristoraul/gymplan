﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace gymplan_rest.Model
{
    public class TrainingPlanExercise
    {
        public int ExerciseId { get; set; }
        public int TrainingPlanId { get; set; }
        public virtual Exercise Exercise { get; set; }
    }
}
